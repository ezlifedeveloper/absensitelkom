<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'report';

    protected $appends = ['day', 'hour', 'name', 'date'];
    protected $with = ['user'];

    public function getDayAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->isoFormat('dddd, D MMMM Y');
    }

    public function getDateAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('d M Y H:i:s');
    }

    public function getHourAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('H:i:s');
    }

    public function getNameAttribute()
    {
        return $this->user->name;
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
