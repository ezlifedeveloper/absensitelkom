<?php

namespace App\Http\Controllers;

use App\Models\Report;
use App\Models\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    //
    public function index(Request $request)
    {
        $from = $request->from ?? false;
        $to = $request->to ?? false;
        $media = $request->media ?? 'web';

        $data = [];
        $check = User::where('id', $request->userid)->first();

        if (!$check) return view('welcome')->with('data', $data);;

        $role = $check->role;
        $id = $request->userid;

        $data = Report::when($from, function ($query) use ($from) {
            return $query->whereDate('created_at', '>=', $from);
        })->when($to, function ($query) use ($to) {
            return $query->whereDate('created_at', '<=', $to);
        })->when($role == 'user', function ($query) use ($id) {
            return $query->where('user_id', $id);
        })->orderBy('created_at', 'desc')->get();

        return view('welcome')->with('data', $data)->with('media', $media);
    }
}
