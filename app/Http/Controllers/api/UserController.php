<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\LogHistory;
use App\Models\Report;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class UserController extends Controller
{
    public function index()
    {
        return response()->json(['success' => false, 'result' => 'Not Authorized']);
    }

    public function store(Request $request)
    {
        if (!$request->filled('username') || !$request->filled('name') || !$request->filled('password') || !$request->filled('role')) {
            return response()->json(['success' => false, 'result' => 'Missing Parameter']);
        }

        $usernameCheck = User::where("username", $request->username)->first();
        if ($usernameCheck) return response()->json(['success' => false, 'result' => 'Username already registered']);

        $data = new User();
        $data->username = $request->username;
        $data->name = $request->name;
        $data->password = bcrypt($request->password);
        $data->role = $request->role ?? 'user';
        $data->save();

        return response()->json(['success' => true, 'result' => 'User Registered', 'data' => $data]);
    }

    public function show(Request $request)
    {
        if (!$request->filled('phoneid') || !$request->filled('username') || !$request->filled('limit')) {
            return response()->json(['success' => false, 'result' => 'Missing Parameter']);
        }

        $limit = $request->limit;
        $check = User::where('username', $request->username)
            ->where('role', 'admin')
            ->first();

        if (!$check) return response()->json(['success' => false, 'result' => 'Not Authorized']);

        $data = User::when($limit > 0, function ($query) use ($limit) {
            return $query->limit($limit)
                ->orderBy('created_at', 'desc');
        })->orderBy('name', 'asc')->get();

        return response()->json(['success' => true, 'result' => 'Get User Succeed', 'data' => $data]);
    }

    public function update(Request $request)
    {
        if (!$request->filled('userid') || !$request->filled('username') || !$request->filled('name') || !$request->filled('role')) {
            return response()->json(['success' => false, 'result' => 'Missing Parameter']);
        }

        $username = $request->username;

        $check = User::where('id', '<>', $request->userid)
            ->where(function ($query) use ($username) {
                return $query->where('username', $username);
            })
            ->first();

        if ($check) return response()->json(['success' => false, 'result' => 'Duplicate Username or MAC']);

        $data = User::where('id', $request->userid)
            ->update([
                'username' => $request->username,
                'name' => $request->name,
                'role' => $request->role,
            ]);

        if ($request->filled('password')) {
            Log::info("update password");
            User::where('id', $request->userid)
                ->update(['password' => bcrypt($request->password)]);
        }

        $data = User::where('id', $request->userid)->first();
        return response()->json(['success' => true, 'result' => 'Data Update Success', 'data' => $data]);
    }

    public function delete(Request $request)
    {
        if (!$request->filled('phoneid') || !$request->filled('username') || !$request->filled('userid')) {
            return response()->json(['success' => false, 'result' => 'Missing Parameter']);
        }

        $check = User::where('username', $request->username)
            // ->where('phone_id', $request->phoneid)
            ->where('role', 'admin')
            ->first();

        if (!$check) return response()->json(['success' => false, 'result' => 'Not Authorized']);

        $deleteReport = Report::where('user_id', $request->userid)->delete();
        $deleteLog = LogHistory::where('user_id', $request->userid)->delete();
        $delete = User::where('id', $request->userid)->delete();

        return response()->json(['success' => true, 'result' => 'Delete User Success']);
    }

    public function reset(Request $request)
    {
        if (!$request->filled('phoneid') || !$request->filled('username') || !$request->filled('userid')) {
            return response()->json(['success' => false, 'result' => 'Missing Parameter']);
        }

        $check = User::where('username', $request->username)
            // ->where('phone_id', $request->phoneid)
            ->where('role', 'admin')
            ->first();

        if (!$check) return response()->json(['success' => false, 'result' => 'Not Authorized']);

        $delete = User::where('id', $request->userid)->update(['phone_id' => NULL]);

        return response()->json(['success' => true, 'result' => 'Reset User Success']);
    }

    public function login(Request $request)
    {
        Log::debug($request);
        if (!$request->filled('username') || !$request->filled('password') || !$request->filled('phoneid') || !$request->filled('lat') || !$request->filled('lng')) {
            return response()->json(['success' => false, 'result' => 'Missing Parameter']);
        }

        $username = $request->username;
        $pwd = $request->password;
        $phoneid = strtolower($request->phoneid);

        $auth = User::where('username', $username)->first();

        if ($auth != null && Hash::check($pwd, $auth->password)) {
            if ($auth->role == 'admin') {
                $this->logAuth($auth, $phoneid, $request->lat, $request->lng, 'login', 'ok');
                // return response()->json(['success' => true, 'result' => 'Login Success', 'data' => $auth]);
            }

            if ($auth->phone_id && $auth->phone_id != $phoneid) {
                $this->logAuth($auth, $phoneid, $request->lat, $request->lng, 'login', 'reject');
                // return response()->json(['success' => false, 'result' => 'Wrong MAC address']);
            } else if ($auth->phone_id) {
                $this->logAuth($auth, $phoneid, $request->lat, $request->lng, 'login', 'ok');
                // return response()->json(['success' => true, 'result' => 'Login Success', 'data' => $auth]);
            } else if (!$auth->phone_id) {
                User::where('username', $username)->update(['phone_id' => $phoneid]);
                $this->logAuth($auth, $phoneid, $request->lat, $request->lng, 'login', 'ok');
                // return response()->json(['success' => true, 'result' => 'Login Success', 'data' => $auth]);
            }

            // return response()->json(['success' => false, 'result' => 'Oops! Something went wrong!']);
        } else if ($auth == null) {
            // return response()->json(['success' => false, 'result' => 'ID not registered']);
        } else if (!Hash::check($pwd, $auth->password)) {
            $this->logAuth($auth, $phoneid, $request->lat, $request->lng, 'login', 'reject');
            // return response()->json(['success' => false, 'result' => 'Password and id combination not match']);
        } else {
            // return response()->json(['success' => false, 'result' => 'Oops! Something went wrong!']);
        }
        return response()->json(['success' => false, 'result' => 'Pengembangan tidak dilanjutkan']);
    }

    private function logAuth($user, $phoneid, $lat, $lng, $type, $status)
    {
        if ($status == 'reject') Log::debug("Login Attempt: " . $user->id . "|" . $lat . "," . $lng . '|' . $phoneid  . "|" . $type);
        LogHistory::create([
            'user_id' => $user->id,
            'lat' => $lat,
            'lng' => $lng,
            'phone_id' => $phoneid,
            'type' => $type,
            'status' => $status,
        ]);
    }

    public function logout(Request $request)
    {
        if (!$request->filled('userid') || !$request->filled('phoneid') || !$request->filled('lat') || !$request->filled('lng')) {
            return response()->json(['success' => false, 'result' => 'Missing Parameter']);
        }

        $userid = $request->userid;
        $phoneid = $request->phoneid;

        $auth = User::where('id', $userid)
            ->where('phone_id', $phoneid)
            ->first();

        if (!$auth) {
            $auth = User::where('id', $userid)->first();
            $this->logAuth($auth, $phoneid, $request->lat, $request->lng, 'logout', 'reject');
            return response()->json(['success' => false, 'result' => 'Logout Failed']);
        }

        $this->logAuth($auth, $phoneid, $request->lat, $request->lng, 'logout', 'ok');
        return response()->json(['success' => true, 'result' => 'Logout Success']);
    }
}
