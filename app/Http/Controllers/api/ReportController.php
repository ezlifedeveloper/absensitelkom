<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\LogHistory;
use App\Models\User;
use App\Models\Report;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Spatie\LaravelImageOptimizer\Facades\ImageOptimizer;

class ReportController extends Controller
{
    public function update(Request $request)
    {
        ini_set('post_max_size', '256M');
        ini_set('upload_max_filesize', '256M');

        if (!$request->filled('userid') || !$request->filled('id') || !$request->filled('keterangan') || !$request->filled('lat') || !$request->filled('lng') || !$request->filled('address')) {
            return response()->json(['success' => false, 'result' => 'Missing Parameter']);
        }

        if ($request->has('photo')) {
            $file = $request->photo;

            $size = $file->getSize();
            $ext = '.' . $file->getClientOriginalExtension();

            $fixedFileName = preg_replace("/[^a-zA-Z0-9.]/", "", $request->userid . '_REPORT');

            $destinationPath = public_path() . '/report/';
            $uploadedname = time() . $fixedFileName . $ext;

            $file->move($destinationPath, $uploadedname);

            $filepath = $destinationPath . $uploadedname;
            // ImageOptimizer::optimize($destinationPath . $uploadedname);

            \Tinify\setKey("CYF8dddtYkTjgFTr69XYhT46lHh8TpFL");
            $source = \Tinify\fromFile($filepath);
            $source->toFile($filepath);

            $destinationPath = env('APP_URL') . '/report/';

            $filepath = $destinationPath . $uploadedname;

            $data = Report::where('id', $request->id)
                ->update([
                    'user_id' => $request->userid,
                    'lat' => $request->lat,
                    'lng' => $request->lng,
                    'photo' => $filepath,
                    'address' => $request->address,
                    'keterangan' => $request->keterangan
                ]);
        } else {
            $data = Report::where('id', $request->id)
                ->update([
                    'user_id' => $request->userid,
                    'lat' => $request->lat,
                    'lng' => $request->lng,
                    'address' => $request->address,
                    'keterangan' => $request->keterangan
                ]);
        }

        return response()->json(['success' => true, 'result' => 'Report Updated', 'data' => $data]);
    }

    public function submit(Request $request)
    {
        ini_set('post_max_size', '256M');
        ini_set('upload_max_filesize', '256M');

        if (!$request->filled('userid') || !$request->filled('keterangan') || !$request->filled('lat') || !$request->filled('lng') || !$request->filled('address') || !$request->has('photo')) {
            return response()->json(['success' => false, 'result' => 'Missing Parameter']);
        }

        $file = $request->photo;

        $size = $file->getSize();
        $ext = '.' . $file->getClientOriginalExtension();

        $fixedFileName = preg_replace("/[^a-zA-Z0-9.]/", "", $request->userid . '_REPORT');

        $destinationPath = public_path() . '/report/';
        $uploadedname = time() . $fixedFileName . $ext;

        $file->move($destinationPath, $uploadedname);

        $filepath = $destinationPath . $uploadedname;
        // ImageOptimizer::optimize($destinationPath . $uploadedname);

        \Tinify\setKey("CYF8dddtYkTjgFTr69XYhT46lHh8TpFL");
        $source = \Tinify\fromFile($filepath);
        $source->toFile($filepath);

        $destinationPath = env('APP_URL') . '/report/';

        $filepath = $destinationPath . $uploadedname;

        $data = new Report();
        $data->user_id = $request->userid;
        $data->lat = $request->lat;
        $data->lng = $request->lng;
        $data->photo = $filepath;
        $data->address = $request->address;
        $data->keterangan = $request->keterangan;
        $data->save();

        return response()->json(['success' => true, 'result' => 'Report Submitted', 'data' => $data]);
    }

    public function show(Request $request)
    {
        if (!$request->filled('userid') || !$request->filled('limit')) {
            return response()->json(['success' => false, 'result' => 'Missing Parameter']);
        }

        $check = User::where('id', $request->userid)->first();

        if (!$check) return response()->json(['success' => false, 'result' => 'Not Authorized']);

        $role = $check->role;
        $id = $request->userid;
        $limit = $request->limit;

        $data = Report::when($role == 'user', function ($query) use ($id) {
            return $query->where('user_id', $id);
        })->when($limit > 0, function ($query) use ($limit) {
            return $query->limit($limit);
        })->orderBy('created_at', 'desc')->get();

        return response()->json(['success' => true, 'result' => 'Get Report Succeed', 'data' => $data]);
    }
}
