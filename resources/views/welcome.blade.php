<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{env('APP_NAME')}}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Font Awesome Icons -->
        <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">

        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

        <!-- daterange picker -->
        <link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker.css') }}">

        <!-- DataTables -->
        <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
        <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">

        <!-- Tempusdominus Bbootstrap 4 -->
        <link rel="stylesheet" href="{{asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">

        <!-- Select2 -->
        <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">

        <!-- summernote -->
        <link rel="stylesheet" href="{{ asset('plugins/summernote/summernote-bs4.css') }}">

        <!-- Theme style -->
        <link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}">

        <!-- overlayScrollbars -->
        <link rel="stylesheet" href="{{asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">

        <!-- Google Font: Source Sans Pro -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    </head>
    <body class="hold-transition layout-top-nav text-sm">
        <div class="wrapper">
            <div class="content-wrapper">
                <br/>
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body">
                                        <table id="datatable-full" style="word-wrap: break-word" class="table table-fixed table-bordered table-striped">
                                            @php $count = 1 @endphp
                                            @if(isset($media) && $media == 'mobile')
                                            <thead align="center" valign="center">
                                                <tr>
                                                    <th>Rekap Laporan</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @forelse ($data as $d)
                                                    <tr>
                                                        <td style="word-break: break-word" >
                                                            {{ $d->name }} <br/>
                                                            {{ $d->date }} <br/>
                                                            {{ $d->lat.', '.$d->lng }} <br/>
                                                            {{ $d->address ?? "-" }} <br/>
                                                            {{ $d->keterangan }} <br/>

                                                            <img src="{{$d->photo}}" style="width: 100px">
                                                        </td>
                                                    </tr>
                                                @empty
                                                    <tr>
                                                        <td>Tidak ada data ditemukan</td>
                                                    </tr>
                                                @endforelse
                                            </tbody>
                                            @else
                                            <thead align="center" valign="center">
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama</th>
                                                    <th>Hari</th>
                                                    <th>Jam</th>
                                                    <th>Detail</th>
                                                    <th>Koordinat</th>
                                                    <th width="120px">Foto</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @forelse ($data as $d)
                                                    <tr>
                                                        <td class="text-center">{{ $count++ }}</td>
                                                        <td style="word-wrap: break-word">{{ $d->name }}</td>
                                                        <td style="word-wrap: break-word">{{ $d->day }}</td>
                                                        <td>{{ $d->hour }}</td>
                                                        <td style="word-wrap: break-word">{{ $d->keterangan }}</td>
                                                        <td style="word-wrap: break-word">{{ $d->lat.', '.$d->lng }}</td>
                                                        <td class="text-center">
                                                            <img src="{{$d->photo}}" style="width: 100px">
                                                        </td>
                                                    </tr>
                                                @empty
                                                    <tr>
                                                        <td class="text-center" colspan="7">Tidak ada data ditemukan</td>
                                                    </tr>
                                                @endforelse
                                            </tbody>
                                            @endif
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
            </div>

            <!-- jQuery -->
            <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>

            <!-- Bootstrap 4 -->
            <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

            <!-- Table -->
            <script src="{{ asset('table/vendor/select2/select2.min.js') }}"></script>
            <script src="{{ asset('table/vendor/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>

            <!-- DataTables -->
            <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
            <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
            <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
            <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>

            <!-- Select2 -->
            <script src="{{ asset('plugins/select2/js/select2.full.min.js') }}"></script>

            <!-- SweetAlert2 -->
            <script src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>

            <!-- InputMask -->
            <script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
            <script src="{{ asset('plugins/inputmask/min/jquery.inputmask.bundle.min.js') }}"></script>

            <!-- date-range-picker -->
            <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>

            <!-- Tempusdominus Bootstrap 4 -->
            <script src="{{ asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>

            <!-- AdminLTE App -->
            <script src="{{ asset('dist/js/adminlte.js') }}"></script>

            <script>
                $(function () {
                    $("#datatable-full").DataTable({
                        "responsive": true,
                        "autoWidth": false,
                        "paging": false
                    });
                });
            </script>
        </div>
    </body>
</html>
