<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', ['as' => 'register', 'uses' => 'UserController@store']);
Route::post('showUser', ['as' => 'showUser', 'uses' => 'UserController@show']);
Route::post('updateUser', ['as' => 'updateUser', 'uses' => 'UserController@update']);
Route::post('deleteUser', ['as' => 'deleteUser', 'uses' => 'UserController@delete']);
Route::post('resetUser', ['as' => 'resetUser', 'uses' => 'UserController@reset']);

Route::post('login', ['as' => 'login', 'uses' => 'UserController@login']);
Route::post('logout', ['as' => 'logout', 'uses' => 'UserController@logout']);

Route::post('submitReport', ['as' => 'submitReport', 'uses' => 'ReportController@submit']);
Route::post('showReport', ['as' => 'showReport', 'uses' => 'ReportController@show']);
Route::post('updateReport', ['as' => 'updateReport', 'uses' => 'ReportController@update']);
