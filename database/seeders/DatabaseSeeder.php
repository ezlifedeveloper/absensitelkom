<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        User::create([
            'username' => 'Davips',
            'name' => 'Davips',
            'role' => 'user',
            'phone_id' => strtolower('C6:CD:36:96:B5:FE'),
            'password' => bcrypt('Davips')
        ]);
        User::create([
            'username' => 'Rio',
            'name' => 'Rio',
            'role' => 'user',
            'phone_id' => strtolower('80:79:5d:04:6a:4f'),
            'password' => bcrypt('Rio')
        ]);
        User::create([
            'username' => 'Fikri',
            'name' => 'Fikri',
            'role' => 'user',
            'phone_id' => strtolower('32:b3:05:58:34:c9'),
            'password' => bcrypt('Fikri')
        ]);
        User::create([
            'username' => 'Dian',
            'name' => 'Dian',
            'role' => 'user',
            'phone_id' => strtolower('9e:88:ef:46:ef:f4'),
            'password' => bcrypt('Dian')
        ]);
        User::create([
            'username' => 'Hepi',
            'name' => 'Hepi',
            'role' => 'user',
            'phone_id' => strtolower('68:BF:C4:AD:A2:E2'),
            'password' => bcrypt('Hepi')
        ]);
        User::create([
            'username' => 'Nita',
            'name' => 'Nita',
            'role' => 'user',
            'phone_id' => strtolower('68:BF:C4:89:6C:5A'),
            'password' => bcrypt('Nita')
        ]);
        User::create([
            'username' => 'Kemal',
            'name' => 'Kemal',
            'role' => 'user',
            'phone_id' => strtolower('0c:98:38:e4:81:17'),
            'password' => bcrypt('Kemal')
        ]);
        User::create([
            'username' => 'Dhita',
            'name' => 'Dhita',
            'role' => 'user',
            'phone_id' => strtolower('68:BF:C4:15:CD:5A'),
            'password' => bcrypt('Dhita')
        ]);
        User::create([
            'username' => 'Dimas',
            'name' => 'Dimas',
            'role' => 'user',
            'phone_id' => strtolower('0C:98:38:8C:57:75'),
            'password' => bcrypt('Dimas')
        ]);
        User::create([
            'username' => 'Apri',
            'name' => 'Apri',
            'role' => 'user',
            'phone_id' => strtolower('e0:1f:88:5e:a2:72'),
            'password' => bcrypt('Apri')
        ]);
        User::create([
            'username' => 'Didik',
            'name' => 'Didik',
            'role' => 'user',
            'phone_id' => strtolower('04:92:26:20:dd:82'),
            'password' => bcrypt('Didik')
        ]);
        User::create([
            'username' => 'Rustin',
            'name' => 'Rustin',
            'role' => 'user',
            'phone_id' => strtolower('8C:F5:A3:1A:F3:17'),
            'password' => bcrypt('Rustin')
        ]);
        User::create([
            'username' => 'Eko',
            'name' => 'Eko',
            'role' => 'user',
            'phone_id' => strtolower('0C:A8:A7:F9:17:9E'),
            'password' => bcrypt('Eko')
        ]);
    }
}
